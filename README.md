# Galopin
It is a library for **Python 3** to search subdomains. It was design to be able of add new searchers quickly, since there are many services that can be helpful in this task.

## Installation

```
git clone https://gitlab.com/Zer1t0/galopin
cd galopin
python3 setup.py install
```

## Examples

In `examples` folder there is an example of the use of the library:
* `subsrch.py` that uses the library to search subdomains


## How to create and add a new searcher

The first step is to create a new subdomain searcher. Here is a helpful template: 
```python
class TemplateSearcher(SubdomainSearcher):

    name = "template_searcher"
    description = "This is a template searcher that does nothing"

    @classmethod
    def _initialize(cls):
        cls._add_argument("api-key", description="Key of the application", type_=str, required=True)
        cls._add_argument("pages", description="Number of pages to search", default=50, type_=int)


    def _start_session(self, **kwargs):
        self._api_key = kwargs["api-key"]
        self._pages = kwargs["pages"]

        # do stuff as login into the service
    
    def _end_session(self):
        # do stuff as logout from service

    def _search_subdomains(self, domain):
        # do the search of subdomains in the service

        for page in self.requested_pages():
            try:
                subdomains = self.extract_subdomains(page)
                # yield subdomains that will be returned to the user asynchronously
                yield subdomains
            except:
                # raise an exception that will be returned to user
                raise Exception("error retrieving subdomains")

        # return a list of subdomains
        return ["www.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

```

You searcher must:

* Inherit from `SubdomainSearcher` class
* provide a `name`
* provide a `description`
* provide a class method `_initialize` to define the service arguments
* provide a method `_start_session` to get the arguments and perform setup actions as login into the service
* provide a method `_end_session` to perform shutdown actions
* provide a method `_search_subdomains` which receives a subdomain as input and returns a list of subdomains

### The `_search_subdomains` method

This method is used to retrieve the subdomains and allows to retrieve the subdomains by using `yield` or `return`. In case of have to iterate a high number of elements maybe `yield` it is a better option since it allows to return the subdomains as soon as are retrieved, allowing to user receive this information sooner.

### How to add you searcher

To add your searcher to the tool and contribute (after test that the implementation is correct), which I appreciate :), then you should just put your python file with your searcher under the directory `galopin\subdomain_searchers`. That's all. Galopin implements a system that detects the searchers at runtime under that folder an uses them automagically.

