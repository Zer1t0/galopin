# Examples

## subsrch.py

Allows the search of subdomains by using `galopin` library.

### Example of use

To search subdomains of `example.com`:
```
python3 subsrch.py search -d example.com
```