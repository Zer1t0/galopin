import argparse
import sys
from threading import Lock
import abc

try:
    import galopin
except ImportError:
    from os import path
    sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '..')))
    import galopin


def parse_args():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")
    subparsers.required = True
    showsearchers_parser = subparsers.add_parser("showsearchers")
    showsearchers_parser.add_argument("--detail", help="Show all searcher details", action='store_true')

    search_parser = subparsers.add_parser("search")
    search_parser.add_argument("-d", "--domain", help="Domain to search subdomains", required=True)
    search_parser.add_argument("-v", "--verbose", help="Verbosity level", action='count', default=0)
    search_parser.add_argument("-s", "--searchers", nargs="+",
                               help="List of searchers to perform the search of subdomains, separated by space")
    search_parser.add_argument("-of", "--output-file", help="File to write the results")
    search_parser.add_argument("-t", "--threads", help="Number of searchers launched simultaneously",
                               type=check_more_than_zero)

    return parser.parse_args()


def check_more_than_zero(value):
    try:
        int_value = int(value)
        if int_value <= 0:
            raise ValueError()
    except ValueError:
        raise argparse.ArgumentTypeError("%s is an invalid positive integer" % value)

    return int_value


def main():
    args = parse_args()

    command = CommandFactory.create(args.command)
    command.main(args)


class CommandFactory:

    @classmethod
    def create(cls, command):
        if command == "search":
            return SearchCommand()
        elif command == "showsearchers":
            return ShowSearchersCommand()
        else:
            raise KeyError("Not found command %s" % command)


class Command:

    def __init__(self):
        self._args = None

    def main(self, args):
        self._args = args
        try:
            self._main()
        except Exception as ex:
            print(ex)
        finally:
            self._close()

    @abc.abstractmethod
    def _main(self):
        raise NotImplementedError()

    def _close(self):
        pass


class ShowSearchersCommand(Command):

    def _main(self):
        print("  Available searchers:\n")
        for searcher_info in self._get_searchers_info_sorted():
            self._print_searcher_info(searcher_info)
        print()

    def _get_searchers_info_sorted(self):
        searchers_names = galopin.subdomain_searchers_names()
        searchers_names.sort()
        for searcher_name in searchers_names:
            yield galopin.subdomain_searcher_information(searcher_name)

    def _print_searcher_info(self, searcher_info):
        print("    %s : %s" % (searcher_info.name, searcher_info.description))
        if self._args.detail:
            self._print_searcher_arguments(searcher_info)

    def _print_searcher_arguments(self, searcher_info):
        for argument_name, argument_definition in searcher_info.arguments.items():
            self._print_searcher_argument(argument_definition)
        print()

    def _print_searcher_argument(self, argument_definition):
        arg_message = " " * 6 + "+ "

        arg_message += "%s" % argument_definition.name
        if argument_definition.mandatory:
            arg_message += "*"
        arg_message += " "

        arg_message += "[%s] " % argument_definition.type.__name__
        if argument_definition.default is not None:
            arg_message += "[=%s] " % argument_definition.default
        if argument_definition.description:
            arg_message += ": %s" % argument_definition.description

        print(arg_message)


class SearchCommand(Command):

    def __init__(self):
        super().__init__()
        self._output_file = None
        self._session = None
        self._threads_count = 0

    def _main(self):
        included_searchers = self._get_included_searchers()
        self._output_file = self._get_output_file()
        self._threads_count = self._get_threads_count()

        results_handler = MyResultsHandler(self._args.verbose, self._output_file)

        self._session = galopin.SearchSession(included=included_searchers)

        if self._args.verbose:
            used_searchers_names = sorted(self._session.searchers_names)
            print("[*] Used searchers: %s" % ", ".join(used_searchers_names))

        self._session.search(self._args.domain, handler=results_handler, threads_count=self._threads_count)

    def _close(self):
        if self._output_file:
            self._output_file.close()

        if self._session:
            self._session.end()

    def _get_included_searchers(self):
        if self._args.searchers:
            self._check_for_invalid_searchers_names()
            return self._args.searchers

        return None

    def _check_for_invalid_searchers_names(self):
        searchers_names = galopin.subdomain_searchers_names()
        for searcher_name in self._args.searchers:
            if searcher_name not in searchers_names:
                raise ValueError("[x] Unknown searcher %s. Use %s showsearchers to get a list of available searchers" %
                                 (searcher_name, sys.argv[0]))

    def _get_output_file(self):
        if self._args.output_file:
            return open(self._args.output_file, "w")
        return None

    def _get_threads_count(self):
        if self._args.threads:
            return self._args.threads
        return len(galopin.subdomain_searchers_names())


class MyResultsHandler(galopin.SearchHandlers):

    def __init__(self, verbose, results_file):
        super().__init__()
        self.on_results = self._on_results
        self.on_error = self._on_error
        self.on_start = self._on_start
        self.on_end = self._on_end
        self.on_complete = self._on_complete

        self._subdomains = []
        self._subdomains_lock = Lock()
        self._verbose = verbose
        self._results_file = results_file

    def _on_start(self, searcher_name):
        if self._verbose:
            print("[*] %s: started" % searcher_name)

    def _on_results(self, searcher_name, subdomains):
        with self._subdomains_lock:
            subdomains_message = ""
            for subdomain in subdomains:
                if not self._is_already_discovered_subdomain(subdomain):
                    self._subdomains.append(subdomain)
                    subdomains_message += subdomain.domain + "\n"

            if self._results_file:
                self._results_file.write(subdomains_message)
            print(subdomains_message, end="")

    def _on_error(self, searcher_name, error):
        print("[x] %s [%s]: %s " % (searcher_name, type(error).__name__, error), file=sys.stderr)

    def _on_end(self, searcher_name):
        if self._verbose:
            print("[*] %s: ended" % searcher_name)

    def _on_complete(self):
        if self._verbose:
            print("[*] Search complete")

    def _is_already_discovered_subdomain(self, subdomain_info):
        for domain_info in self._subdomains:
            if subdomain_info.domain == domain_info.domain:
                return True
        return False


if __name__ == '__main__':
    main()
