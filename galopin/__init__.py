from .subdomain_searcher.arguments import ArgumentError
from .handlers import SearchHandlers

from ._setup_registry import _setup_registry, _setup_registry_from_file

from .funcs import *

_setup_registry()
