from .subdomain_searcher.registry import SubdomainSearcherRegistry
from .subdomain_searcher.loader import SubdomainSearcherLoader

import galopin.globals


def _setup_registry():
    _setup_registry_from_directory(galopin.globals.subsearchers_directory)


def _setup_registry_from_directory(directory):
    galopin.globals.registry = SubdomainSearcherRegistry()

    loader = SubdomainSearcherLoader(galopin.globals.registry)
    loader.from_directory(directory)


def _setup_registry_from_file(filepath):
    galopin.globals.registry = SubdomainSearcherRegistry()

    loader = SubdomainSearcherLoader(galopin.globals.registry)
    loader.from_file(filepath)
