import galopin.globals
from .subdomain_searcher_session import SearchSession


def subdomain_searchers_names():
    return galopin.globals.registry.subdomain_searchers_names()


def subdomain_searcher_information(subdomain_searcher_name):
    return galopin.globals.registry.subdomain_searcher_information(subdomain_searcher_name)


def search_subdomains(domain, arguments={}, included=[], excluded=[], handler=None, proxies=None, threads_count=1):
    session = SearchSession(arguments=arguments, included=included, excluded=excluded, proxies=proxies)
    results = session.search(domain, handler=handler, threads_count=threads_count)
    session.end()

    return results
