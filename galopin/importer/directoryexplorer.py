import os


class InvalidDirectory(Exception):

    def __init__(self, directory_name):
        super(InvalidDirectory, self).__init__("Invalid directory %s" % directory_name)
        self._directory_name = directory_name

    @property
    def directory_name(self):
        return self._directory_name


class DirectoryExplorer:

    def __init__(self, directory):
        self._directory = directory

    def list_python_files(self):
        try:
            for filename in os.listdir(self._directory):
                filepath = os.path.join(self._directory, filename)
                if os.path.isfile(filepath) and filepath.endswith(".py"):
                    yield filepath
        except FileNotFoundError:
            raise InvalidDirectory(self._directory)