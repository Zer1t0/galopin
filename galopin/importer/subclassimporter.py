import types
import importlib
import importlib.machinery

from galopin.utils.classlist import *
from .directoryexplorer import *


class ImportationError(Exception):
    pass


class SubClassImporter:

    def __init__(self, parent_class):
        self._parent_class = parent_class

    def import_from_directory(self, directory):
        try:
            return self._import_from_directory(directory)
        except InvalidDirectory as ex:
            raise ImportationError("Directory %s is invalid" % ex.directory_name)
        except ClassAlreadyExistsError as ex:
            raise ImportationError("Subclass %s was repeated" % ex.class_name)

    def import_from_file(self, filepath):
        subclasses = ClassList()
        file_subclasses = self._try_import_from_file(filepath)
        subclasses.extend(file_subclasses)
        return subclasses

    def _import_from_directory(self, directory):
        subclasses = ClassList()
        directory_explorer = DirectoryExplorer(directory)
        for python_file_path in directory_explorer.list_python_files():
            file_subclasses = self._try_import_from_file(python_file_path)
            subclasses.extend(file_subclasses)
        return subclasses

    def _try_import_from_file(self, filepath):
        try:
            return self._import_from_file(filepath)
        except SyntaxError as ex:
            raise ImportationError("Syntax error in %s: %s" % (filepath, str(ex)))
        except FileNotFoundError:
            raise ImportationError("File %s was not found" % filepath)

    def _import_from_file(self, filepath):
        module = self._import_python_module(filepath)
        subclasses = self._extract_subclasses_in_module(module)
        return subclasses

    def _import_python_module(self, filepath):
        loader = importlib.machinery.SourceFileLoader(filepath, filepath)
        mod = types.ModuleType(loader.name)
        loader.exec_module(mod)
        return mod

    def _extract_subclasses_in_module(self, module):
        return [subclass for subclass in module.__dict__.values() if self._is_subclass(subclass)]

    def _is_subclass(self, subclass):
        try:
            return issubclass(subclass, self._parent_class) and subclass != self._parent_class
        except TypeError:
            return False
