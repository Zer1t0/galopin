
class ArgumentError(Exception):
    pass


class SubdomainSearcherArgumentDefinition:

    def __init__(self, name, description="", required=False, type_=str, default=None):
        if type(name) is not str:
            raise TypeError("name must be str")
        if type(description) is not str:
            raise TypeError("description must be str")
        if type(required) is not bool:
            raise TypeError("mandatory must be bool")
        if type(type_) is not type:
            raise TypeError("type_ must be a type")
        if default is not None and type(default) is not type_:
            raise TypeError("default must be a %s" % type_.__name__)

        self.name = name
        self.description = description
        self.mandatory = required
        self.type = type_
        self.default = default


class SubdomainSearcherArgumentParser:

    def __init__(self):
        self._arguments_definitions = {}

    def add(self, argument):
        if argument in self:
            raise KeyError("argument %s already defined" % argument.name)
        self._arguments_definitions[argument.name] = argument

    def __contains__(self, argument):
        return argument.name in self._arguments_definitions

    @property
    def arguments(self):
        return self._arguments_definitions

    def parse_args(self, arguments={}):
        parsed_arguments = {}
        for arg_name in self._arguments_definitions:
            parsed_arguments[arg_name] = self._parse_arg(arguments, arg_name)
        return parsed_arguments

    def _parse_arg(self, arguments, arg_name):
        try:
            return self._parse_arg_in_arguments(arg_name, arguments[arg_name])
        except KeyError:
            return self._parse_arg_not_in_arguments(arg_name)

    def _parse_arg_in_arguments(self, arg_name, arg_value):
        type_ = self._arguments_definitions[arg_name].type
        if type(arg_value) is not type_:
            raise ArgumentError("Argument %s must be %s" % (arg_name, type_.__name__))
        return arg_value

    def _parse_arg_not_in_arguments(self, arg_name):
        if self._arguments_definitions[arg_name].mandatory:
            raise ArgumentError("Required argument %s missed" % arg_name)
        return self._arguments_definitions[arg_name].default
