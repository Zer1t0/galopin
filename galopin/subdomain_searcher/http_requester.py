import requests

DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0"


class HTTPRequester:

    def __init__(self, proxies=None, user_agent=DEFAULT_USER_AGENT):
        self._session = requests.Session()
        self._session.headers.update({
            "User-Agent": user_agent
        })

        self._verify_ssl = True
        self._proxies = proxies

    def get(self, url, **kwargs):
        if "verify" not in kwargs:
            kwargs["verify"] = self._verify_ssl

        return self._session.get(url, proxies=self._proxies, **kwargs)

    def post(self, url, **kwargs):
        if "verify" not in kwargs:
            kwargs["verify"] = self._verify_ssl

        return self._session.post(url, proxies=self._proxies, **kwargs)
