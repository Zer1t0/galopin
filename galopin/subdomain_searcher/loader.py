from ..importer.subclassimporter import *
from .subdomainsearcher import *


class SubdomainSearcherLoader:

    def __init__(self, registry):
        self._registry = registry

    def from_directory(self, directory):
        importer = SubClassImporter(SubdomainSearcher)
        searchers = importer.import_from_directory(directory)

        for searcher in searchers:
            self._registry.add(searcher)

    def from_file(self, filepath):
        importer = SubClassImporter(SubdomainSearcher)
        searchers = importer.import_from_file(filepath)

        for searcher in searchers:
            self._registry.add(searcher)
