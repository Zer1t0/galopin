from ..utils.classlist import *

from threading import Lock


class AlreadyRegisteredError(Exception):
    pass


class SubdomainSearcherRegistry:

    def __init__(self):
        self._subdomain_searchers = ClassList()

    def add(self, subdomain_searcher_class):
        try:
            self._try_add(subdomain_searcher_class)
        except ClassAlreadyExistsError:
            raise AlreadyRegisteredError("Subdomain Searcher %s:%s is already registered"
                                         % (subdomain_searcher_class.__name__, subdomain_searcher_class.name))

    def _try_add(self, subdomain_searcher_class):
        if self._is_subdomain_searcher_registered(subdomain_searcher_class.name):
            raise AlreadyRegisteredError("Subdomain Searcher %s:%s is already registered"
                                         % (subdomain_searcher_class.__name__, subdomain_searcher_class.name))
        self._subdomain_searchers.append(subdomain_searcher_class)
        subdomain_searcher_class.initialize()

    def _is_subdomain_searcher_registered(self, subdomain_searcher_name):
        return subdomain_searcher_name in self.subdomain_searchers_names()

    def subdomain_searchers_names(self):
        return [c.name for c in self._subdomain_searchers]

    def subdomain_searcher_information(self, subdomain_searcher_name):
        for searcher in self._subdomain_searchers:
            if searcher.name == subdomain_searcher_name:
                return searcher.information()

        raise KeyError("Not found subdomain searcher %s")

    def instanciate(self, subdomain_searcher_name, proxies=None, arguments={}):
        return self._subdomain_searcher(subdomain_searcher_name)(arguments=arguments, proxies=proxies)

    def _subdomain_searcher(self, subdomain_searcher_name):
        for searcher in self._subdomain_searchers:
            if searcher.name == subdomain_searcher_name:
                return searcher

        raise KeyError("Not found subdomain searcher %s")
