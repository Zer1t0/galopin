from types import GeneratorType


class DomainInfo:

    def __init__(self, domain=""):
        self.domain = domain


class SubdomainSearch:

    def __init__(self, search_method, domain, on_results, on_error):
        self._search_subdomains = search_method
        self._domain = domain
        self._on_error = on_error
        self._on_results = on_results
        self._total_subdomains_infos = []
        self._error = None

    def on_error(self, error):
        self._error = error
        if self._on_error:
            self._on_error(error)

    def on_results(self, results):
        if self._on_results:
            self._on_results(results)

    def search_subdomains(self):
        try:
            self._search_and_process_subdomains_results()
        except Exception as error:
            self.on_error(error)

        return self._total_subdomains_infos, self._error

    def _search_and_process_subdomains_results(self):
        subdomains_result = self._search_subdomains(self._domain)
        self._process_subdomains(subdomains_result)

    def _process_subdomains(self, subdomains_result):
        if type(subdomains_result) is GeneratorType:
            self._process_subdomains_generator(subdomains_result)
        else:
            self._process_subdomains_list_or_str(subdomains_result)

    def _process_subdomains_generator(self, subdomains_generator):
        for subdomains_result in subdomains_generator:
            self._process_subdomains_list_or_str(subdomains_result)

    def _process_subdomains_list_or_str(self, subdomains):
        if type(subdomains) is str:
            subdomains_list = [subdomains]
        else:
            subdomains_list = subdomains
        self._process_subdomains_list(subdomains_list)

    def _process_subdomains_list(self, subdomains_list):
        new_subdomains = self._filter_subdomains(subdomains_list)
        if new_subdomains:
            news_domains_infos = []
            for subdomain in new_subdomains:
                news_domains_infos.append(DomainInfo(subdomain))
            self._total_subdomains_infos.extend(news_domains_infos)
            self.on_results(news_domains_infos)

    def _filter_subdomains(self, subdomains):
        real_subdomains = [subdomain for subdomain in subdomains if subdomain.endswith("." + self._domain)]
        new_subdomains = [subdomain for subdomain in real_subdomains if not self._discovered_subdomain(subdomain)]
        new_subdomains = list(set(new_subdomains))

        return new_subdomains

    def _discovered_subdomain(self, subdomain):
        for subdomain_info in self._total_subdomains_infos:
            if subdomain == subdomain_info.domain:
                return True
        return False

