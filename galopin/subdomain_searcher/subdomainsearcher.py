import abc

from .arguments import *
from .http_requester import *
from .subdomain_search import SubdomainSearch


class SubdomainSearcherInfo:

    def __init__(self):
        self.name = ""
        self.description = ""
        self.arguments = {}


class SubdomainSearcher:
    """
    It's the abstract base class for subsearchers
    """

    name = "Base"
    description = "Base searcher which defines the searchers structures"
    __argument_parser = None  # not initialize map here due to aliasing problems

    @classmethod
    def initialize(cls):
        cls.__argument_parser = SubdomainSearcherArgumentParser()
        return cls._initialize()

    @classmethod
    @abc.abstractmethod
    def _initialize(cls):
        raise NotImplementedError()

    @classmethod
    def _add_argument(cls, name, description="", required=False, type_=str, default=None):
        cls.__argument_parser.add(SubdomainSearcherArgumentDefinition(name, description, required, type_, default))

    @classmethod
    def information(cls):
        info = SubdomainSearcherInfo()
        info.name = cls.name
        info.description = cls.description
        info.arguments = cls.__argument_parser.arguments
        return info

    def __init__(self, proxies=None, arguments={}):
        self._http_requester = HTTPRequester(proxies=proxies)
        parsed_arguments = self.__argument_parser.parse_args(arguments)
        self._in_session = False

        self.start_session(**parsed_arguments)

    def start_session(self, **kwargs):
        if not self._in_session:
            self._start_session(**kwargs)
            self._in_session = True

    @abc.abstractmethod
    def _start_session(self, **kwargs):
        """
        Method to initialize subsearcher configurations. Example: init session on some service
        """
        raise NotImplementedError()

    def search_subdomains(self, domain, on_results=None, on_error=None):
        search = SubdomainSearch(self._search_subdomains, domain, on_results, on_error)
        return search.search_subdomains()

    @abc.abstractmethod
    def _search_subdomains(self, domain):
        """
        Main method used to perform the search of subdomains
        """
        raise NotImplementedError()

    def end_session(self):
        if self._in_session:
            self._in_session = False
            self._end_session()

    @abc.abstractmethod
    def _end_session(self):
        """
        Method to do close operations before destroy the instance. Ex: close session in some service
        """
        raise NotImplementedError()

    def _get(self, url, **kwargs):
        return self._http_requester.get(url, **kwargs)

    def _post(self, url, **kwargs):
        return self._http_requester.post(url, **kwargs)
