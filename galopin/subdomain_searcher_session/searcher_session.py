from threading import Thread
from concurrent.futures import ThreadPoolExecutor
import concurrent.futures

import galopin.globals
from galopin.subdomain_searcher.arguments import ArgumentError


class SearchSession:

    def __init__(self, arguments={}, included=[], excluded=[], proxies=None):
        self._subdomain_searchers = {}
        subdomain_searchers_names = self._filter_subdomain_searchers_names(included, excluded)
        for subdomain_searcher_name in subdomain_searchers_names:
            self._try_load_subdomain_searcher(subdomain_searcher_name, arguments.get(subdomain_searcher_name, {}),
                                              included, proxies)

    def _filter_subdomain_searchers_names(self, included, excluded):
        if included:
            return self._filter_subdomain_searchers_names_by_included(included)
        else:
            return self._filter_subdomain_searchers_names_by_excluded(excluded)

    def _filter_subdomain_searchers_names_by_included(self, included):
        return list(set(included))

    def _filter_subdomain_searchers_names_by_excluded(self, excluded):
        subdomain_searchers_names = set(galopin.globals.registry.subdomain_searchers_names())
        subdomain_searchers_names = [s_name for s_name in subdomain_searchers_names if s_name not in excluded]
        return subdomain_searchers_names

    def _try_load_subdomain_searcher(self, subdomain_searcher_name, arguments, included, proxies):
        try:
            self._load_subdomain_searcher(subdomain_searcher_name, arguments, proxies)
        except ArgumentError as ex:
            if subdomain_searcher_name in included or arguments:
                self.end()
                raise ex

    def _load_subdomain_searcher(self, subdomain_searcher_name, arguments, proxies):
        self._subdomain_searchers[subdomain_searcher_name] = galopin.globals.registry.instanciate(
            subdomain_searcher_name, arguments=arguments, proxies=proxies)

    def __del__(self):
        self.end()

    @property
    def searchers_names(self):
        return list(self._subdomain_searchers.keys())

    def search(self, domain, handler=None, threads_count=1):
        pool = self._initiate_search_pool(domain, handler, threads_count)
        results = self._recollect_search_results(pool)

        if handler and handler.on_complete:
            handler.on_complete()

        return results

    def _initiate_search_pool(self, domain, handler, threads_count):
        pool = ThreadPoolExecutor(threads_count)
        threads = []

        for subdomain_searcher_name in self.searchers_names:
            thread = pool.submit(self._search_subdomains_with_searcher,
                                 self._subdomain_searchers[subdomain_searcher_name], domain, handler)
            threads.append(thread)

        return threads

    def _recollect_search_results(self, pool):
        results = {}
        for f in concurrent.futures.as_completed(pool):
            subdomain_searcher_name, subdomains, error = f.result()
            results[subdomain_searcher_name] = {
                "subdomains": subdomains,
                "error": error
            }

        return results

    def _search_subdomains_with_searcher(self, subdomain_searcher, domain, handler):
        executor = SearcherExecutor(subdomain_searcher, domain, handler)
        return executor.search()

    def end(self):
        for subdomain_searcher_name in self.searchers_names:
            self._subdomain_searchers[subdomain_searcher_name].end_session()


class SearcherExecutor:

    def __init__(self, subdomain_searcher, domain, handlers):
        self._subdomain_searcher = subdomain_searcher
        self._domain = domain
        self._handlers = handlers

    def on_results(self, results):
        if self._handlers and self._handlers.on_results:
            Thread(target=self._handlers.on_results, args=(self._subdomain_searcher.name, results)).start()

    def on_error(self, error):
        if self._handlers and self._handlers.on_error:
            Thread(target=self._handlers.on_error, args=(self._subdomain_searcher.name, error)).start()

    def search(self):

        if self._handlers and self._handlers.on_start:
            Thread(target=self._handlers.on_start, args=(self._subdomain_searcher.name,)).start()

        subdomains, error = self._subdomain_searcher.search_subdomains(self._domain, on_results=self.on_results,
                                                                       on_error=self.on_error)

        if self._handlers and self._handlers.on_end:
            Thread(target=self._handlers.on_end, args=(self._subdomain_searcher.name,)).start()

        return self._subdomain_searcher.name, subdomains, error
