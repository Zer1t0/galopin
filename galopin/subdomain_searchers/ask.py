from galopin.subdomain_searcher import SubdomainSearcher
from galopin.subdomain_searcher.exceptions import CaptchaError
import re

from bs4 import BeautifulSoup


class AskSearcher(SubdomainSearcher):

    name = "Ask"
    description = "Retrieve domains using ask"

    @classmethod
    def _initialize(cls):
        cls._add_argument("pages", description="Number of pages to search", default=50, type_=int)

    def _start_session(self, **kwargs):
        self._pages = kwargs["pages"]

    def _search_subdomains(self, domain):
        url_base = "https://www.ask.com/web"
        search = "site:%s" % domain

        for i in range(0, self._pages):
            params = {
                "q": search,
                "o": 0,
                "l": "dir",
                "qo": "pagination",
                "qsrc": 998
            }

            if i > 0:
                params["page"] = i + 1

            response = self._get(url_base, params=params)

            if self._is_captcha_in_response(response):
                raise CaptchaError()

            if self._is_empty_page(response):
                break

            yield self._found_subdomains_in_response(response, domain)

    def _end_session(self):
        pass

    @staticmethod
    def _is_captcha_in_response(response):
        return False

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains

    @staticmethod
    def _is_empty_page(response):
        soup = BeautifulSoup(response.text, "html.parser")
        return soup.find("li", class_="PartialWebPagination-next") is None
