
from galopin.subdomain_searcher import SubdomainSearcher
from galopin.subdomain_searcher.exceptions import CaptchaError
import re

from bs4 import BeautifulSoup


class BaiduSearcher(SubdomainSearcher):

    name = "Baidu"
    description = "Retrieve domains using baidu"

    @classmethod
    def _initialize(cls):
        cls._add_argument("pages", description="Number of pages to search", default=50, type_=int)

    def _start_session(self, **kwargs):
        self._pages = kwargs["pages"]

    def _search_subdomains(self, domain):
        url_base = "https://www.baidu.com/s"
        search = "site:%s" % domain

        for i in range(0, self._pages):
            start_result = i * 10
            params = {
                "wd": search
            }

            if start_result:
                params["pn"] = start_result

            response = self._get(url_base, params=params)

            if self._is_captcha_in_response(response):
                raise CaptchaError()

            found_subdomains = self._found_subdomains_in_response(response, domain)

            yield found_subdomains

            if not self._has_next_page(response):
                break

    def _end_session(self):
        pass

    @staticmethod
    def _is_captcha_in_response(response):
        # TODO : I was not able to be banned by baidu to implement this :'(
        return False

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains

    @staticmethod
    def _has_next_page(response):
        soup = BeautifulSoup(response.text, "html.parser")
        div_page = soup.find("div", id="page")
        try:
            a = div_page.findChildren("a", recursive=False)[-1]
            return "n" in a["class"]
        except (KeyError, IndexError):
            return False
