from galopin.subdomain_searcher import SubdomainSearcher
from galopin.subdomain_searcher.exceptions import CaptchaError
import re
import urllib.parse
from bs4 import BeautifulSoup


class BingSearcher(SubdomainSearcher):

    name = "Bing"
    description = "Retrieve domains using bing"

    @classmethod
    def _initialize(cls):
        cls._add_argument("pages", description="Number of pages to search", default=50, type_=int)

    def _start_session(self, **kwargs):
        self._pages = kwargs["pages"]

    def _search_subdomains(self, domain):
        url_base = "https://www.bing.com/search"
        search = "site:%s" % domain

        for i in range(0, self._pages):
            start_result = i * 10
            params = {
                "q": search
            }

            if start_result:
                params["first"] = start_result + 1

            response = self._get(url_base, params=params)

            if self._is_captcha_in_response(response):
                raise CaptchaError()

            found_subdomains = self._found_subdomains_in_response(response, domain)
            yield found_subdomains

            if not self._has_next_page(response):
                break

    def _end_session(self):
        pass

    @staticmethod
    def _is_captcha_in_response(response):
        # TODO : I was not able to be banned by bing to implement this :'(
        return False

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, urllib.parse.unquote(response.text))

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains

    @staticmethod
    def _has_next_page(response):
        soup = BeautifulSoup(response.text, "html.parser")
        return soup.find("div", class_="sw_next") is not None
