
from galopin.subdomain_searcher import SubdomainSearcher
import re


class DNSdumpsterSearcher(SubdomainSearcher):

    name = "DNSdumpster"
    description = "Retrieve domains using DNSdumpster.com"

    @classmethod
    def _initialize(cls):
        pass

    def _start_session(self, **kwargs):
        pass

    def _search_subdomains(self, domain):
        url_base = "https://dnsdumpster.com/"

        response = self._get(url_base)
        csrf_token = response.cookies["csrftoken"]

        data = {
            "targetip": domain,
            "csrfmiddlewaretoken": csrf_token
        }
        headers = {
            "Referer": "https://dnsdumpster.com/"
        }

        response = self._post(url_base, data=data, headers=headers)
        subdomains = self._found_subdomains_in_response(response, domain)

        return subdomains

    def _end_session(self):
        pass

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains
