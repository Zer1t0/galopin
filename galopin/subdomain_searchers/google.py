
from galopin.subdomain_searcher import SubdomainSearcher
from galopin.subdomain_searcher.exceptions import CaptchaError
import re

from bs4 import BeautifulSoup


class GoogleSearcher(SubdomainSearcher):

    name = "Google"
    description = "Retrieve domains using google"

    @classmethod
    def _initialize(cls):
        cls._add_argument("pages", description="Number of pages to search", default=50, type_=int)

    def _start_session(self, **kwargs):
        self._pages = kwargs["pages"]

    def _search_subdomains(self, domain):
        url_base = "https://www.google.com/search"
        search = "site:%s" % domain

        for i in range(0, self._pages):
            start_result = i * 10
            params = {
                "q": search,
                "oq": search,
                "client": "firefox-b-d"
            }

            if start_result:
                params["start"] = start_result

            response = self._get(url_base, params=params)

            if self._is_captcha_in_response(response):
                raise CaptchaError()

            yield self._found_subdomains_in_response(response, domain)

            if not self._has_next_page(response):
                break

    def _end_session(self):
        pass

    @staticmethod
    def _is_captcha_in_response(response):
        return response.status_code != 200

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains

    @staticmethod
    def _has_next_page(response):
        soup = BeautifulSoup(response.text, "html.parser")
        return soup.find("a", id="pnnext") is not None
