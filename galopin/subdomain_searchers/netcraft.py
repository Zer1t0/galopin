
from galopin.subdomain_searcher import SubdomainSearcher
import re
import urllib.parse
import hashlib
from bs4 import BeautifulSoup


class NetcraftSearcher(SubdomainSearcher):

    name = "Netcraft"
    description = "Retrieve domains using netcraft"

    @classmethod
    def _initialize(cls):
        pass

    def _start_session(self, **kwargs):
        pass

    def _search_subdomains(self, domain):
        url_base = "https://searchdns.netcraft.com"

        params = {
            "restriction": "site ends with",
            "host": domain
        }
        url = url_base
        response = self._get(url, params=params)

        while 1:
            challenge = response.cookies["netcraft_js_verification_challenge"]

            resolved = self._resolve_challenge(challenge)
            cookies = {
                "netcraft_js_verification_response": resolved
            }

            response = self._get(url, params=params, cookies=cookies)
            found_subdomains = self._found_subdomains_in_response(response, domain)
            yield found_subdomains

            next_url = self._next_url_from_response(response)

            if not next_url:
                break

            url = url_base + next_url
            response = self._get(url)

    def _end_session(self):
        pass

    @staticmethod
    def _resolve_challenge(challenge):
        unquoted_challenge = urllib.parse.unquote(challenge)
        return hashlib.sha1(unquoted_challenge.encode()).hexdigest()

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains

    @staticmethod
    def _next_url_from_response(response):
        soup = BeautifulSoup(response.text, "html.parser")
        a_next = soup.find("a", string="Next page")

        if a_next:
            return a_next["href"]

        return None
