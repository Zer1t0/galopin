
from galopin.subdomain_searcher import SubdomainSearcher
import re
import random


class PTRarchiveSearcher(SubdomainSearcher):

    name = "PTRarchive"
    description = "Retrieve domains by using PTRarchive"

    @classmethod
    def _initialize(cls):
        pass

    def _start_session(self, **kwargs):
        pass

    def _search_subdomains(self, domain):
        url_base = "http://ptrarchive.com/tools/search3.htm"

        params = {
            "date": "ALL",
            "label": domain
        }
        cookies = {
            "pa_id": str(random.randint(0, 100000000))
        }

        response = self._get(url_base, params=params, cookies=cookies)

        return self._found_subdomains_in_response(response, domain)

    def _end_session(self):
        pass

    @staticmethod
    def _found_subdomains_in_response(response, domain):
        domain_regex = "(([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,})"
        matches = re.findall(domain_regex, response.text)

        if matches:
            subdomains = [m[0] for m in matches if m[0].endswith("." + domain)]
            subdomains = list(set(subdomains))
        else:
            subdomains = []

        return subdomains
