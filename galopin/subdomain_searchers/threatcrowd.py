import json

from galopin.subdomain_searcher import SubdomainSearcher


class ThreatCrowdSearcher(SubdomainSearcher):

    name = "ThreatCrowd"
    description = "Retrieve domains from https://www.threatcrowd.org"

    @classmethod
    def _initialize(cls):
        pass

    def _start_session(self, **kwargs):
        pass

    def _search_subdomains(self, domain):
        url = "https://www.threatcrowd.org/searchApi/v2/domain/report/"
        get_params = {
            "domain": domain
        }

        resp = self._get(url, params=get_params)
        subdomains = self._parse_response(resp, domain)

        return subdomains

    def _end_session(self):
        pass

    @staticmethod
    def _parse_response(resp, domain):
        subdomains = []

        try:
            resp_json = json.loads(resp.text)

            for subdomain in resp_json["subdomains"]:
                if subdomain.endswith(domain):
                    subdomains.append(subdomain)
        except (KeyError, json.JSONDecodeError):
            pass

        return subdomains