from bs4 import BeautifulSoup
from galopin.subdomain_searcher import SubdomainSearcher
from galopin.subdomain_searcher.exceptions import CaptchaError


class VirusTotalSearcher(SubdomainSearcher):

    name = "VirusTotal"
    description = "Retrieve domains from https://www.virustotal.com/"

    @classmethod
    def _initialize(cls):
        pass

    def _start_session(self, **kwargs):
        pass

    def _search_subdomains(self, domain):
        url = "https://www.virustotal.com/en/domain/%s/information/" % domain

        resp = self._get(url)

        if resp.status_code == 403:
            raise CaptchaError()

        subdomains = self._parse_response(resp, domain)

        return subdomains

    def _end_session(self):
        pass

    @staticmethod
    def _parse_response(resp, domain):
        subdomains = []

        soup = BeautifulSoup(resp.text, "html.parser")
        subdomains_div_soup = soup.find("div", id="observed-subdomains")

        if subdomains_div_soup:
            for a_soup in subdomains_div_soup.find_all("a"):
                subdomain = a_soup.text.strip()
                if subdomain.endswith(domain):
                    subdomains.append(subdomain)

        return subdomains
