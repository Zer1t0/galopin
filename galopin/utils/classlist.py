
class ClassAlreadyExistsError(Exception):

    def __init__(self, class_name):
        super(ClassAlreadyExistsError, self).__init__("Class %s already exists" % class_name)
        self._class_name = class_name

    @property
    def class_name(self):
        return self._class_name


class ClassList:

    def __init__(self):
        self._classes = []

    def extend(self, new_classes):
        for new_class in new_classes:
            self.append(new_class)

    def append(self, new_class):
        if new_class in self:
            raise ClassAlreadyExistsError(new_class.__name__)
        self._classes.append(new_class)

    def names(self):
        return [c.__name__ for c in self._classes]

    def __contains__(self, item):
        return item in self._classes or item.__name__ in self.names()

    def __iter__(self):
        return iter(self._classes)

    def __len__(self):
        return len(self._classes)

    def __getitem__(self, class_name):
        for c in self._classes:
            if c.__name__ == class_name:
                return c
