import os
import sys

from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = 'galopin'
DESCRIPTION = 'Library to search subdomains'
URL = 'https://gitlab.com/Zer1t0/galopin'
EMAIL = 'Zer1t0@hotmail.com'
AUTHOR = 'Zer1t0'
REQUIRES_PYTHON = '>=3.5.3'
VERSION = '0.1.0'

parent_dir_path = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(parent_dir_path, 'requirements.txt'), encoding='utf-8') as f:
        required_packages = [line for line in f]

REQUIRED = required_packages

EXTRAS = {}

try:
    with open(os.path.join(parent_dir_path, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION


print (find_packages(exclude=["tests", "tests.*", "examples"]))


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=["tests", "tests.*", "examples"]),
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license='AGPL-3.0',
)
