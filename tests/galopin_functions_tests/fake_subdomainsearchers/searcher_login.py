from galopin.subdomain_searcher import subdomainsearcher
from galopin import ArgumentError


class SubdomainSearcherLogin(subdomainsearcher.SubdomainSearcher):

    name = "SearcherLogin"
    description = "This is the subdomain searcher with a fake login"

    correct_user = "admin"
    correct_password = "admin"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user", required=True)
        cls._add_argument("password", required=True)

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

    def _start_session(self, **kwargs):
        self.user = kwargs["user"]
        self.password = kwargs["password"]

        if kwargs["user"] != self.correct_user or kwargs["password"] != self.correct_password:
            raise ArgumentError("Invalid user or password")

    def _end_session(self):
        pass
