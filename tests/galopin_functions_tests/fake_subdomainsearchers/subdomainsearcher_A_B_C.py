from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherA(subdomainsearcher.SubdomainSearcher):

    name = "SearcherA"
    description = "This is the subdomain searcher A"

    subdomain_prefix = "A."

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return [
            self.subdomain_prefix + "www.%s" % domain,
            self.subdomain_prefix + "mail.%s" % domain,
            "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass


class SubdomainSearcherB(subdomainsearcher.SubdomainSearcher):

    name = "SearcherB"
    description = "This is the subdomain searcher B"

    subdomain_prefix = "B."

    @classmethod
    def _initialize(cls):
        cls._add_argument("user", required=True)

    def _search_subdomains(self, domain, info_callback=None):
        return [
            self.subdomain_prefix + "www.%s" % domain,
            self.subdomain_prefix + "mail.%s" % domain,
            "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass


class SubdomainSearcherC(subdomainsearcher.SubdomainSearcher):

    name = "SearcherC"
    description = "This is the subdomain searcher C"

    subdomain_prefix = "C."

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return [
            self.subdomain_prefix + "www.%s" % domain,
            self.subdomain_prefix + "mail.%s" % domain,
            "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
