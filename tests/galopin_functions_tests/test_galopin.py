import unittest
import os
from time import sleep
from threading import Lock

import galopin
from galopin import ArgumentError, SearchHandlers

from tests.utils import call_on_port_connect

from tests.galopin_functions_tests.fake_subdomainsearchers.subdomainsearcher_A_B_C import *
from tests.galopin_functions_tests.fake_subdomainsearchers.searcher_login import *
from tests.galopin_functions_tests.fake_subdomainsearchers.searcher_error_in_search import error_in_search_message

current_directory = os.path.dirname(os.path.realpath(__file__))
custom_files_directory = current_directory + '/fake_subdomainsearchers'


class GalopinTest(unittest.TestCase):

    def setUp(self):
        self.domain = "domain.test"

    def test_get_subdomain_searchers_names(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        names = galopin.subdomain_searchers_names()

        self.assertIn(SubdomainSearcherA.name, names)
        self.assertIn(SubdomainSearcherB.name, names)

    def test_get_subdomain_searcher_information(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        searcher_information = galopin.subdomain_searcher_information(SubdomainSearcherA.name)

        self.assertEqual(SubdomainSearcherA.name, searcher_information.name)
        self.assertEqual(SubdomainSearcherA.description, searcher_information.description)

        searcher_arguments = searcher_information.arguments
        self.assertIn("user", searcher_arguments)

    def test_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        results = galopin.search_subdomains(self.domain)

        self.assertNotEqual(0, len(results))
        for subdomain_searcher_name in results:
            self.assertNotEqual(0, len(results[subdomain_searcher_name]["subdomains"]))
            for subdomain in results[subdomain_searcher_name]["subdomains"]:
                self.assertTrue(subdomain.domain.endswith("." + self.domain))

    def test_search_subdomains_return_errors(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_error_in_search.py')
        results = galopin.search_subdomains(self.domain)

        self.assertEqual(1, len(results))
        for subdomain_searcher_name in results:
            error = results[subdomain_searcher_name]["error"]
            self.assertEqual(error_in_search_message, str(error))

    def test_search_with_mandatory_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        user_B = "userb"
        arguments = {
            SubdomainSearcherB.name: {
                "user": user_B
            }
        }
        results = galopin.search_subdomains(self.domain, arguments=arguments)

        self.assertIn(SubdomainSearcherB.name, results)

    def test_search_with_incorrect_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_login.py')
        arguments = {
            SubdomainSearcherLogin.name: {
                "user": "wrong_user",
                "password": "wrong_password"
            }
        }
        self.assertRaises(ArgumentError, galopin.search_subdomains, self.domain, arguments=arguments)

    def test_search_only_with_included(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')

        results = galopin.search_subdomains(self.domain, included=[SubdomainSearcherA.name])

        self.assertIn(SubdomainSearcherA.name, results)
        self.assertNotIn(SubdomainSearcherB.name, results)
        self.assertNotIn(SubdomainSearcherC.name, results)

    def test_search_with_included_searchers_without_mandatory_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        self.assertRaises(ArgumentError, galopin.search_subdomains, self.domain, included=[SubdomainSearcherB.name])

    def test_not_search_with_excluded(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        results = galopin.search_subdomains(self.domain, excluded=[SubdomainSearcherA.name])

        self.assertNotIn(SubdomainSearcherA.name, results)
        self.assertIn(SubdomainSearcherC.name, results)

    def test_search_with_excluded_included_same_searcher(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        results = galopin.search_subdomains(self.domain, excluded=[SubdomainSearcherA.name],
                                            included=[SubdomainSearcherA.name])

        self.assertIn(SubdomainSearcherA.name, results)

    def test_call_on_complete_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A.py')
        self._complete = False

        def on_complete():
            self._complete = True

        handlers = SearchHandlers()
        handlers.on_complete = on_complete
        results = galopin.search_subdomains(self.domain, handler=handlers)
        sleep(0.3)  # to let threads call on_complete
        self.assertTrue(self._complete)

    def test_call_on_start_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')

        self._ended = []
        self._lock = Lock()
        self._lock.acquire()

        def on_start(searcher_name):
            self._ended.append(searcher_name)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_start = on_start
        handlers.on_complete = on_complete

        galopin.search_subdomains(self.domain, handler=handlers)
        self._lock.acquire(timeout=3)

        self.assertIn(SubdomainSearcherA.name, self._ended)
        self.assertIn(SubdomainSearcherC.name, self._ended)
        self.assertNotIn(SubdomainSearcherB.name, self._ended)

    def test_call_on_end_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')

        self._ended = []
        self._lock = Lock()
        self._lock.acquire()

        def on_end(searcher_name):
            self._ended.append(searcher_name)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_end = on_end
        handlers.on_complete = on_complete

        galopin.search_subdomains(self.domain, handler=handlers)
        self._lock.acquire(timeout=3)

        self.assertIn(SubdomainSearcherA.name, self._ended)
        self.assertIn(SubdomainSearcherC.name, self._ended)
        self.assertNotIn(SubdomainSearcherB.name, self._ended)

    def test_call_on_results_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A.py')

        self._results = {}
        self._lock = Lock()
        self._lock.acquire()

        def on_results(searcher_name, subdomains):
            if searcher_name not in self._results:
                self._results[searcher_name] = []

            self._results[searcher_name].extend(subdomains)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_results = on_results
        handlers.on_complete = on_complete

        galopin.search_subdomains(self.domain, handler=handlers)
        self._lock.acquire(timeout=3)

        self.assertNotEqual(0, len(self._results))
        for subdomain_searcher_name in self._results:
            self.assertNotEqual(0, len(self._results[subdomain_searcher_name]))
            for subdomain in self._results[subdomain_searcher_name]:
                self.assertTrue(subdomain.domain.endswith("." + self.domain))

    def test_call_on_error_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_error.py')

        self._errored = False
        self._lock = Lock()
        self._lock.acquire()

        def on_error(searcher_name, error):
            self._errored = True

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_error = on_error
        handlers.on_complete = on_complete

        galopin.search_subdomains(self.domain, handler=handlers)
        self._lock.acquire(timeout=3)

        self.assertTrue(self._errored)

    def test_use_http_proxie(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_http_proxy.py')
        proxy_port = 4949
        proxies = {"http": "http://127.0.0.1:%d" % proxy_port}
        self._http_proxie_used = False

        def proxie_call():
            self._http_proxie_used = True

        call_on_port_connect(4949, proxie_call, timeout=1)

        galopin.search_subdomains(self.domain, proxies=proxies)
        sleep(0.3)  # to let searchers call proxie
        self.assertTrue(self._http_proxie_used)

    def test_use_http_proxie(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_https_proxy.py')
        proxy_port = 4949
        proxies = {"https": "http://127.0.0.1:%d" % proxy_port}
        self._http_proxie_used = False

        def proxie_call():
            self._http_proxie_used = True

        call_on_port_connect(4949, proxie_call, timeout=1)

        galopin.search_subdomains(self.domain, proxies=proxies)
        sleep(0.3)  # to let searchers call proxie
        self.assertTrue(self._http_proxie_used)


if __name__ == '__main__':
    unittest.main()
