import unittest
from galopin.subdomain_searcher.subdomainsearcher import SubdomainSearcher
from galopin.importer import *

from .fake_subsearchers.subsearchers_A_B import *
from .fake_subsearchers.subsearcher_C import *

current_directory = os.path.dirname(os.path.realpath(__file__))
custom_files_directory = current_directory + '/fake_python_files'


class SubClassImporterTest(unittest.TestCase):

    def setUp(self):
        self._importer = SubClassImporter(SubdomainSearcher)


class SubClassImporterDirectoryTest(SubClassImporterTest):

    def test_import_good_directory(self):
        subclasses = self._importer.import_from_directory(current_directory + '/fake_subsearchers')

        self.assertIn(SubSearcherA, subclasses)
        self.assertIn(SubSearcherB, subclasses)
        self.assertIn(SubSearcherC, subclasses)

    def test_import_directory_with_repeated_classes(self):
        self.assertRaises(ImportationError, self._importer.import_from_directory,
                          current_directory + '/fake_repeated_subsearchers')

    def test_import_directory_with_syntax_error_files(self):
        self.assertRaises(ImportationError, self._importer.import_from_directory,
                          current_directory + '/fake_syntax_error_subsearchers')

    def test_import_directory_without_classes(self):
        subclasses = self._importer.import_from_directory(
            current_directory + '/fake_no_subsearchers')
        self.assertEqual(0, len(subclasses))

    def test_import_not_in_subdirectories(self):
        subclasses = self._importer.import_from_directory(
            current_directory + '/fake_subdirectories_subsearchers')

        self.assertIn(SubSearcherA, subclasses)
        self.assertNotIn(SubSearcherB, subclasses)

    def test_import_for_nonexistent_directory(self):
        self.assertRaises(ImportationError, self._importer.import_from_directory,
                          current_directory + '/fake_invalid_directory_subsearchers')

    def test_import_only_from_python_files(self):
        subclasses = self._importer.import_from_directory(
            current_directory + '/fake_subsearchers_with_no_python_files')

        self.assertIn(SubSearcherA, subclasses)
        self.assertNotIn(SubSearcherB, subclasses)
        self.assertNotIn(SubSearcherC, subclasses)


class SubClassImporterFileTest(SubClassImporterTest):

    def test_good_file(self):
        subclasses = self._importer.import_from_file(
            custom_files_directory + '/subsearchers_A_B.py')

        self.assertIn(SubSearcherA, subclasses)
        self.assertIn(SubSearcherB, subclasses)

    def test_nonexistent_file(self):
        self.assertRaises(ImportationError, self._importer.import_from_file,
                          custom_files_directory + '/fake_invalid_file_subsearchers.py')

    def test_import_syntax_error_file(self):
        self.assertRaises(ImportationError, self._importer.import_from_file,
                          custom_files_directory + '/subsearcher_A_malformed.py')

    def test_import_file_with_repeated_subclasses(self):
        subclasses = self._importer.import_from_file(
            custom_files_directory + '/subsearcher_A_repeated.py')

        self.assertEqual(1, len(subclasses))
        self.assertIn(SubSearcherA, subclasses)

    def test_import_file_without_subclasses(self):
        importer = SubClassImporter(SubdomainSearcher)
        subclasses = importer.import_from_file(
            custom_files_directory + '/no_subsearchers.py')

        self.assertEqual(0, len(subclasses))


if __name__ == '__main__':
    unittest.main()
