from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherA(subdomainsearcher.SubdomainSearcher):

    name = "SearcherA"
    description = "This is the subdomain searcher A"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass


class SubdomainSearcherB(subdomainsearcher.SubdomainSearcher):

    name = "SearcherB"
    description = "This is the subdomain searcher B"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
