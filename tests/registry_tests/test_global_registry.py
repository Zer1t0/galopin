import unittest

from galopin.subdomain_searcher.subdomainsearcher import SubdomainSearcher
from galopin import globals


class GlobalRegistryTest(unittest.TestCase):

    def test_registry_setup(self):
        self.assertNotEqual(0, len(globals.registry._subdomain_searchers))
        for subdomainsearcher_class in globals.registry._subdomain_searchers:
            self.assertTrue(issubclass(subdomainsearcher_class, SubdomainSearcher))


if __name__ == '__main__':
    unittest.main()
