import unittest

from threading import Thread


from galopin.subdomain_searcher.registry import *
from .fake_subdomainsearchers.subdomainsearcher_A_B import *
from .fake_subdomainsearchers.subdomainsearcher_copy_A import *


class SubdomainSearcherRegistryTest(unittest.TestCase):

    def test_add_subdomain_searcher(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)

        self.assertEqual(SubdomainSearcherA,
                         registry._subdomain_searchers[SubdomainSearcherA.__name__])

    def test_add_repeated_subdomain_searcher_class(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)
        self.assertRaises(AlreadyRegisteredError, registry.add, SubdomainSearcherA)

    def test_add_subdomain_searcher_class_with_repeated_name(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)
        self.assertRaises(AlreadyRegisteredError, registry.add, SubdomainSearcherCopyA)

    def test_subdomain_searcher_names(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)
        registry.add(SubdomainSearcherB)

        searcher_names = registry.subdomain_searchers_names()

        self.assertIn(SubdomainSearcherA.name, searcher_names)
        self.assertIn(SubdomainSearcherB.name, searcher_names)

    def test_subdomain_searcher_information(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)

        searcher_information = registry.subdomain_searcher_information(SubdomainSearcherA.name)

        self.assertEqual(SubdomainSearcherA.name, searcher_information.name)
        self.assertEqual(SubdomainSearcherA.description, searcher_information.description)

        searcher_arguments = searcher_information.arguments
        self.assertIn("user", searcher_arguments)

    def test_subdomain_searcher_instance(self):
        registry = SubdomainSearcherRegistry()
        registry.add(SubdomainSearcherA)

        searcher_a = registry.instanciate(SubdomainSearcherA.name)

        self.assertTrue(isinstance(searcher_a, SubdomainSearcherA))

    def test_subdomain_searcher_instance_nonexistent(self):
        registry = SubdomainSearcherRegistry()
        self.assertRaises(KeyError, registry.instanciate, SubdomainSearcherB.name)


class SubdomainSearcherRegistryThreadingTest(unittest.TestCase):

    def test_add_threaded(self):
        registry = SubdomainSearcherRegistry()
        self.register_error = False

        def add_subdomain_searcher_A():
            try:
                registry.add(SubdomainSearcherA)
            except AlreadyRegisteredError:
                self.register_error = True

        for i in range(1000):
            registry = SubdomainSearcherRegistry()
            self.register_error = False

            threads = []
            for j in range(2):
                t = Thread(target=add_subdomain_searcher_A)
                threads.append(t)

            for t in threads:
                t.start()

            for t in threads:
                t.join()

            if not self.register_error:
                self.fail("Same class registered twice")


if __name__ == '__main__':
    unittest.main()
