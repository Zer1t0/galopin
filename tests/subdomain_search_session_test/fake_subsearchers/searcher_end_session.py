from galopin.subdomain_searcher import subdomainsearcher

ended_session_message = "Session ended"


class SubdomainSearcherEndSession(subdomainsearcher.SubdomainSearcher):

    name = "SearcherEndSession"
    description = "This is the subdomain searcher to test the end of session"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        subdomains = ["www.%s" % domain, "mail.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

        for subdomain in subdomains:
            yield subdomain

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        raise Exception(ended_session_message)
