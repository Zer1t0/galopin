from galopin.subdomain_searcher import subdomainsearcher

error_in_search_message = "Error in search"


class SubdomainSearcherErrorSearch(subdomainsearcher.SubdomainSearcher):

    name = "SearcherErrorSearch"
    description = "This is the subdomain searcher which raises an error in search"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        raise Exception(error_in_search_message)

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
