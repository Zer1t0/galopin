from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherHTTPProxy(subdomainsearcher.SubdomainSearcher):

    name = "SearcherHTTPProxy"
    description = "This is the subdomain searcher which uses http"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        try:
            self._get("http://localhost:4444")
        except:
            pass

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
