from galopin.subdomain_searcher import subdomainsearcher


started_session_message = "Session started"

class SubdomainSearcherStartSession(subdomainsearcher.SubdomainSearcher):

    name = "SearcherStartSession"
    description = "This is the subdomain searcher to test the start of session"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        subdomains = ["www.%s" % domain, "mail.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

        for subdomain in subdomains:
            yield subdomain

    def _start_session(self, **kwargs):
        raise Exception(started_session_message)

    def _end_session(self):
        global A_IN_SESSION
        A_IN_SESSION = False