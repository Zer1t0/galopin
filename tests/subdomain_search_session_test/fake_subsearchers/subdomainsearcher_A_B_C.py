from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherA(subdomainsearcher.SubdomainSearcher):

    name = "SearcherA"
    description = "This is the subdomain searcher A"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.A.%s" % domain, "mail.A.%s" % domain, "%s.A.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass


class SubdomainSearcherB(subdomainsearcher.SubdomainSearcher):

    name = "SearcherB"
    description = "This is the subdomain searcher B"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user", required=True)

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.B.%s" % domain, "mail.B.%s" % domain, "%s.B.com" % domain]

    def _start_session(self, **kwargs):
        self.user = kwargs["user"]

    def _end_session(self):
        pass


class SubdomainSearcherC(subdomainsearcher.SubdomainSearcher):

    name = "SearcherC"
    description = "This is the subdomain searcher C"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.C.%s" % domain, "mail.C.%s" % domain, "%s.C.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
