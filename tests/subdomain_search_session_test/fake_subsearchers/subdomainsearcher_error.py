from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherError(subdomainsearcher.SubdomainSearcher):

    name = "SearcherError"
    description = "This is the subdomain searcher which raises an error"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        raise Exception("Fake error")

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
