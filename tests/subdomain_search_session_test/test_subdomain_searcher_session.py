import unittest
import os
from time import sleep
from threading import Lock

from galopin import ArgumentError
from galopin.subdomain_searcher_session.searcher_session import SearchSession
from galopin.handlers import SearchHandlers
import galopin


from tests.utils import call_on_port_connect

from .fake_subsearchers.subdomainsearcher_A_B_C import *
from .fake_subsearchers.searcher_login import *
from .fake_subsearchers.searcher_start_session import started_session_message
from .fake_subsearchers.searcher_end_session import ended_session_message
from .fake_subsearchers.searcher_error_in_search import error_in_search_message

sessions = {"start_session": False}

current_directory = os.path.dirname(os.path.realpath(__file__))
custom_files_directory = current_directory + '/fake_subsearchers'


class SearchSessionTest(unittest.TestCase):

    def test_load_only_non_mandatory_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B.py')
        session = SearchSession()

        self.assertIn(SubdomainSearcherA.name, session.searchers_names)
        self.assertNotIn(SubdomainSearcherB.name, session.searchers_names)

    def test_pass_mandatory_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B.py')
        user_B = "userb"
        arguments = {
            SubdomainSearcherB.name: {
                "user": user_B
            }
        }
        session = SearchSession(arguments=arguments)

        self.assertEqual(user_B, session._subdomain_searchers[SubdomainSearcherB.name].user)

    def test_pass_incorrect_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_login.py')
        arguments = {
            SubdomainSearcherLogin.name: {
                "user": "wrong_user",
                "password": "wrong_password"
            }
        }
        self.assertRaises(ArgumentError, SearchSession, arguments=arguments)

    def test_load_only_included_searchers(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession(included=[SubdomainSearcherA.name])

        self.assertIn(SubdomainSearcherA.name, session.searchers_names)
        self.assertNotIn(SubdomainSearcherC.name, session.searchers_names)

    def test_load_included_searchers_without_mandatory_arguments(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        self.assertRaises(ArgumentError, SearchSession, included=[SubdomainSearcherB.name])

    def test_not_load_included_searcher_twice(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession(included=[SubdomainSearcherA.name, SubdomainSearcherA.name])

        apparitions_of_A = 0
        for name in session.searchers_names:
            if name == SubdomainSearcherA.name:
                apparitions_of_A += 1

        self.assertEqual(1, apparitions_of_A)

    def test_not_load_excluded_searchers(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession(excluded=[SubdomainSearcherA.name])

        self.assertNotIn(SubdomainSearcherA.name, session.searchers_names)
        self.assertNotIn(SubdomainSearcherB.name, session.searchers_names)
        self.assertIn(SubdomainSearcherC.name, session.searchers_names)

    def test_excluded_included_same_searcher(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession(excluded=[SubdomainSearcherA.name], included=[SubdomainSearcherA.name])

        self.assertIn(SubdomainSearcherA.name, session.searchers_names)

    def test_search_subdomains(self):
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A.py')
        session = SearchSession()

        domain = "domain.test"

        results = session.search(domain)

        self.assertNotEqual(0, len(results))
        for subdomain_searcher_name in results:
            self.assertNotEqual(0, len(results[subdomain_searcher_name]["subdomains"]))
            for subdomain in results[subdomain_searcher_name]["subdomains"]:
                self.assertTrue(subdomain.domain.endswith("." + domain))

    def test_search_subdomains_return_errors(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_error_in_search.py')
        session = SearchSession()

        domain = "domain.test"

        results = session.search(domain)

        self.assertEqual(1, len(results))
        for subdomain_searcher_name in results:
            error = results[subdomain_searcher_name]["error"]
            self.assertEqual(error_in_search_message, str(error))

    def test_call_on_complete_search_subdomains(self):
        domain = "domain.com"
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A.py')
        session = SearchSession()

        self._complete = False

        def on_complete():
            self._complete = True

        handlers = SearchHandlers()
        handlers.on_complete = on_complete

        session.search(domain, handlers)
        sleep(0.3)  # to let threads call on_complete
        self.assertTrue(self._complete)

    def test_call_on_start_search_subdomains(self):
        domain = "domain.com"
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession()

        self._ended = []
        self._lock = Lock()
        self._lock.acquire()

        def on_start(searcher_name):
            self._ended.append(searcher_name)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_start = on_start
        handlers.on_complete = on_complete

        session.search(domain, handlers)
        self._lock.acquire(timeout=3)

        self.assertIn(SubdomainSearcherA.name, self._ended)
        self.assertIn(SubdomainSearcherC.name, self._ended)
        self.assertNotIn(SubdomainSearcherB.name, self._ended)

    def test_call_on_end_search_subdomains(self):
        domain = "domain.com"
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A_B_C.py')
        session = SearchSession()

        self._ended = []
        self._lock = Lock()
        self._lock.acquire()

        def on_end(searcher_name):
            self._ended.append(searcher_name)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_end = on_end
        handlers.on_complete = on_complete

        session.search(domain, handlers)
        self._lock.acquire(timeout=3)

        self.assertIn(SubdomainSearcherA.name, self._ended)
        self.assertIn(SubdomainSearcherC.name, self._ended)
        self.assertNotIn(SubdomainSearcherB.name, self._ended)

    def test_call_on_results_search_subdomains(self):
        domain = "domain.com"
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_A.py')
        session = SearchSession()

        self._results = {}
        self._lock = Lock()
        self._lock.acquire()

        def on_results(searcher_name, subdomains):
            if searcher_name not in self._results:
                self._results[searcher_name] = []

            self._results[searcher_name].extend(subdomains)

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_results = on_results
        handlers.on_complete = on_complete

        session.search(domain, handlers)
        self._lock.acquire(timeout=3)

        self.assertNotEqual(0, len(self._results))
        for subdomain_searcher_name in self._results:
            self.assertNotEqual(0, len(self._results[subdomain_searcher_name]))
            for subdomain in self._results[subdomain_searcher_name]:
                self.assertTrue(subdomain.domain.endswith("." + domain))

    def test_call_on_error_search_subdomains(self):
        domain = "domain.com"
        galopin._setup_registry_from_file(custom_files_directory + '/subdomainsearcher_error.py')
        session = SearchSession()

        self._errored = False
        self._lock = Lock()
        self._lock.acquire()

        def on_error(searcher_name, error):
            self._errored = True

        def on_complete():
            self._lock.release()

        handlers = SearchHandlers()
        handlers.on_error = on_error
        handlers.on_complete = on_complete

        session.search(domain, handlers)
        self._lock.acquire(timeout=3)

        self.assertTrue(self._errored)

    def test_start_subdomain_searchers_session(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_start_session.py')
        try:
            SearchSession()
            self.fail("Not exception of session started raised")
        except Exception as ex:
            self.assertEqual(started_session_message, str(ex))

    def test_end_subdomain_searchers_session(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_end_session.py')
        try:
            session = SearchSession()
            session.end()
            self.fail("Not exception of session ended raised")
        except Exception as ex:
            self.assertEqual(ended_session_message, str(ex))

    def test_use_http_proxie(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_http_proxy.py')
        proxy_port = 4949
        proxies = {"http": "http://127.0.0.1:%d" % proxy_port}
        self._http_proxie_used = False

        def proxie_call():
            self._http_proxie_used = True

        call_on_port_connect(4949, proxie_call, timeout=1)

        session = SearchSession(proxies=proxies)
        session.search("domain.test")
        sleep(0.3)  # to let searchers call proxie
        self.assertTrue(self._http_proxie_used)

    def test_use_https_proxie(self):
        galopin._setup_registry_from_file(custom_files_directory + '/searcher_https_proxy.py')
        proxy_port = 4949
        proxies = {"https": "http://127.0.0.1:%d" % proxy_port}
        self._http_proxie_used = False

        def proxie_call():
            self._http_proxie_used = True

        call_on_port_connect(4949, proxie_call, timeout=1)

        session = SearchSession(proxies=proxies)
        session.search("domain.test")
        sleep(0.3)  # to let searchers call proxie
        self.assertTrue(self._http_proxie_used)


if __name__ == '__main__':
    unittest.main()
