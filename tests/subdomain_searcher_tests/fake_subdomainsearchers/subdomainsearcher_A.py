from galopin.subdomain_searcher import subdomainsearcher

IN_SESSION = False


class SubdomainSearcherA(subdomainsearcher.SubdomainSearcher):

    name = "SearcherA"
    description = "This is the subdomain searcher A"

    @classmethod
    def _initialize(cls):
        cls._add_argument("user")

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.%s" % domain, "mail.%s" % domain, "%s.com" % domain]

    def _start_session(self, **kwargs):
        global IN_SESSION
        IN_SESSION = True

    def _end_session(self):
        global IN_SESSION
        IN_SESSION = False
