from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherNone(subdomainsearcher.SubdomainSearcher):

    name = "SearcherNone"
    description = "This is the subdomain searcher which returns None"

    @classmethod
    def _initialize(cls):
        pass

    def _search_subdomains(self, domain, info_callback=None):
        return None

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
