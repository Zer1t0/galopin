from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherException(subdomainsearcher.SubdomainSearcher):

    name = "SearcherException"
    description = "This is the subdomain searcher which raise exception in middle of yields"

    @classmethod
    def _initialize(cls):
        pass

    def _search_subdomains(self, domain, info_callback=None):
        yield ["www.%s" % domain, "mail.%s" %domain]
        raise Exception("Fake error")

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
