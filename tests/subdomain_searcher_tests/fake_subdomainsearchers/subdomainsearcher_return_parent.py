from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherReturnParent(subdomainsearcher.SubdomainSearcher):

    name = "ReturnParent"
    description = "Fake searcher that returns the parent domain"

    @classmethod
    def _initialize(cls):
        pass

    def _search_subdomains(self, domain, info_callback=None):
        return ["www.%s" % domain, domain, "%s.com" % domain]

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
