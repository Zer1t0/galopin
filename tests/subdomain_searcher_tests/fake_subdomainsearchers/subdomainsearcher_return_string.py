from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherReturnString(subdomainsearcher.SubdomainSearcher):

    name = "SearcherReturnString"
    description = "This is the subdomain searcher which returns an string"

    @classmethod
    def _initialize(cls):
        pass

    def _search_subdomains(self, domain, info_callback=None):
        return "www.%s" % domain

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass


class SubdomainSearcherYieldString(subdomainsearcher.SubdomainSearcher):

    name = "SearcherYieldString"
    description = "This is the subdomain searcher which yields an string"

    @classmethod
    def _initialize(cls):
        pass

    def _search_subdomains(self, domain, info_callback=None):
        yield "www.%s" % domain

    def _start_session(self, **kwargs):
        pass

    def _end_session(self):
        pass
