import unittest

from .fake_subdomainsearchers.subdomainsearcher_A import SubdomainSearcherA
from .fake_subdomainsearchers import subdomainsearcher_A
from .fake_subdomainsearchers.subdomainsearcher_None import SubdomainSearcherNone
from .fake_subdomainsearchers.subdomainsearcher_result_exception import SubdomainSearcherException
from .fake_subdomainsearchers.subdomainsearcher_return_string import *
from .fake_subdomainsearchers.subdomainsearcher_return_parent import SubdomainSearcherReturnParent


class SubdomainSearcherTest(unittest.TestCase):

    def setUp(self):
        SubdomainSearcherA.initialize()
        SubdomainSearcherNone.initialize()
        SubdomainSearcherException.initialize()
        SubdomainSearcherReturnString.initialize()
        SubdomainSearcherYieldString.initialize()
        SubdomainSearcherReturnParent.initialize()
        subdomainsearcher_A.IN_SESSION = False

    def test_get_information(self):
        searcher_information = SubdomainSearcherA.information()

        self.assertEqual(SubdomainSearcherA.name, searcher_information.name)
        self.assertEqual(SubdomainSearcherA.description, searcher_information.description)

        searcher_arguments = searcher_information.arguments
        self.assertIn("user", searcher_arguments)

    def test_start_session(self):
        searcher = SubdomainSearcherA()
        self.assertTrue(subdomainsearcher_A.IN_SESSION)

    def test_end_session(self):
        searcher = SubdomainSearcherA()
        searcher.end_session()
        self.assertFalse(subdomainsearcher_A.IN_SESSION)

    def test_get_subdomains_from_parent_domain(self):
        searcher = SubdomainSearcherA()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)

        self.assertTrue(len(subdomains) != 0)
        for subdomain in subdomains:
            self.assertTrue(subdomain.domain.endswith("." + domain),
                            msg="%s doesn't ends with %s" % (subdomain.domain, domain))

    def test_not_get_parent_domain(self):
        searcher = SubdomainSearcherReturnParent()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)

        self.assertTrue(len(subdomains) != 0)
        for subdomain in subdomains:
            self.assertNotEqual(domain, subdomain.domain)

    def test_get_empty_subdomain_array_from_None(self):
        searcher = SubdomainSearcherNone()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)
        self.assertEqual([], subdomains)

    def test_get_subdomains_and_error(self):
        searcher = SubdomainSearcherException()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)
        self.assertNotEqual(0, len(subdomains))
        self.assertNotEqual(None, error)

    def test_one_subdomain_array_from_return_string(self):
        searcher = SubdomainSearcherReturnString()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)
        self.assertEqual(1, len(subdomains))

    def test_one_subdomain_array_from_yield_string(self):
        searcher = SubdomainSearcherYieldString()
        domain = "test.unit"
        subdomains, error = searcher.search_subdomains(domain)
        self.assertEqual(1, len(subdomains))

    def test_get_results_in_callbacks(self):
        searcher = SubdomainSearcherA()
        domain = "test.unit"
        self._results_returned = False

        def on_results(subdomains):
            for subdomain in subdomains:
                self.assertTrue(subdomain.domain.endswith("." + domain),
                                msg="%s doesn't ends with %s" % (subdomain.domain, domain))

                self._results_returned = True

        searcher.search_subdomains(domain, on_results=on_results)
        self.assertTrue(self._results_returned)





if __name__ == '__main__':
    unittest.main()
