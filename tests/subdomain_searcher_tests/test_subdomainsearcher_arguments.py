import unittest

from galopin.subdomain_searcher.arguments import *


class SubdomainSearcherArgumentParserTest(unittest.TestCase):

    def test_get_arguments(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user"))
        arg_parser.add(SubdomainSearcherArgumentDefinition("password"))

        arguments = arg_parser.arguments
        self.assertIn("user", arguments)
        self.assertIn("password", arguments)

    def test_parse_required_argument(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user", required=True))

        self.assertRaises(ArgumentError, arg_parser.parse_args)
        arguments = arg_parser.parse_args({"user": "admin"})
        self.assertEqual("admin", arguments["user"])

    def test_parse_argument_with_default_value(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user", default="admin"))

        arguments = arg_parser.parse_args()
        self.assertEqual("admin", arguments["user"])

    def test_parse_incorrect_type_argument(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user", default="admin"))

        self.assertRaises(ArgumentError, arg_parser.parse_args, {"user": 1})

    def test_parse_non_required_without_default_value(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user"))

        arguments = arg_parser.parse_args()
        self.assertEqual(None, arguments["user"])

    def test_parse_many_arguments(self):
        arg_parser = SubdomainSearcherArgumentParser()
        arg_parser.add(SubdomainSearcherArgumentDefinition("user"))
        arg_parser.add(SubdomainSearcherArgumentDefinition("password"))

        arguments = arg_parser.parse_args()
        self.assertEqual(None, arguments["user"])
        self.assertEqual(None, arguments["password"])


if __name__ == '__main__':
    unittest.main()
