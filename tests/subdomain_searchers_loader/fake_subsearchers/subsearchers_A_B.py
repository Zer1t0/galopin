from galopin.subdomain_searcher import subdomainsearcher


class SubdomainSearcherA(subdomainsearcher.SubdomainSearcher):

    name = "SearcherA"

    @classmethod
    def _initialize(cls):
        pass


class SubdomainSearcherB(subdomainsearcher.SubdomainSearcher):

    name = "SearcherB"

    @classmethod
    def _initialize(cls):
        pass
