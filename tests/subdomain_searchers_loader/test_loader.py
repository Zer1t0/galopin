import unittest
import os
from galopin.subdomain_searcher.registry import *
from galopin.subdomain_searcher.loader import *

from .fake_subsearchers.subsearchers_A_B import *
from .fake_subsearchers.subsearcher_C import *

current_directory = os.path.dirname(os.path.realpath(__file__))


class SubdomainSearcherLoaderTest(unittest.TestCase):

    def test_load_from_directory(self):
        registry = SubdomainSearcherRegistry()
        loader = SubdomainSearcherLoader(registry)
        loader.from_directory(current_directory + '/fake_subsearchers')

        searcher_names = registry.subdomain_searchers_names()

        self.assertIn(SubdomainSearcherA.name, searcher_names)
        self.assertIn(SubdomainSearcherB.name, searcher_names)
        self.assertIn(SubdomainSearcherC.name, searcher_names)

    def test_load_from_file(self):
        registry = SubdomainSearcherRegistry()
        loader = SubdomainSearcherLoader(registry)
        loader.from_file(current_directory + '/fake_subsearchers/subsearchers_A_B.py')

        searcher_names = registry.subdomain_searchers_names()
        self.assertIn(SubdomainSearcherA.name, searcher_names)
        self.assertIn(SubdomainSearcherB.name, searcher_names)
