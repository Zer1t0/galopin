import unittest
from .importer_tests.test_subclassimporter import *
from .subdomain_searcher_tests.test_subdomainsearcher_arguments import *
from .subdomain_searcher_tests.test_subdomainsearcher import *
from .registry_tests.test_registry import *
from .registry_tests.test_global_registry import *
from .subdomain_search_session_test.test_subdomain_searcher_session import *
from .galopin_functions_tests.test_galopin import *

if __name__ == '__main__':
    unittest.main()
