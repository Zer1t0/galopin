import socket
from threading import Thread


def call_on_port_connect(port, func, timeout=2):
    t = Thread(target=_call_on_port_connect, args=(port, func, timeout))
    t.start()
    return t


def _call_on_port_connect(port, func, timeout):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('localhost', port))
    sock.listen(1)
    sock.settimeout(timeout)
    try:
        s, addr = sock.accept()
        func()
        s.close()
    except socket.timeout:
        pass
    sock.close()
